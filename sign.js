$("#submit").click(function () {
  let flag = true;
  const fname = $("#fname").val();
  const lname = $("#lname").val();
  const email = $("#email").val();
  const phno = $("#number").val();
  const gender = $('input[name="gender"]:checked').val();
  const state = $("#state option:selected").val();
  const password = $("#password").val();
  const cpassword = $("#cpassword").val();

  validatefname();
  validatelname();
  validatephno();
  validategender();
  validatestate();
  validatepassword();
  validatecpassword();
  validateterms();

  function validatefname() {
    var abc = /^[A-Za-z]+$/;
    var Valid = abc.test(fname);
    if (!Valid) {
      $("#drf").text("name should only contain alphabets");
      flag = false;
    }
  }

  function validatelname() {
    var abc = /^[A-Za-z]+$/;
    var Valid = abc.test(lname);
    if (!Valid) {
      $("#error2").html("name should only contain alphabets");
      flag = false;
    }
  }
  function validatephno() {
    var ph = /^[0-9-+]+$/;
    var dash = ph.test(phno);
    if (!dash || ph.length >= 10) {
      $("#error4").html("phone number is greater than 10 number");
      flag = false;
    }
  }
  function validategender() {
    var valid = $("input[name=gender]").is(":checked");
    if (!valid) {
      $("#error5").html("Select a gender");
      flag = false;
    }
  }
  function validatestate() {
    if (state == " ") {
      $("#error6").html("Select one state");
      flag = false;
    }
  }
  function validatepassword() {
    if (password.length == "") {
      $("#error7").html("Enter a password");
      flag = false;
    } else if (password.length < 9) {
      $("#error7").html("Password must contain 10 characters");
      flag = false;
    }
  }
  function validatecpassword() {
    if (cpassword.length == "") {
      $("#error8").html("Enter a confirm password");
      flag = false;
    } else if (password != cpassword) {
      $("#error8").html("Password didn't match");
      flag = false;
    }
  }
  function validateterms() {
    var s = $("#terms").prop("checked");
    if (!s) {
      $("#terms").html("Tick box to agree");
      flag = false;
    }
  }
  if (flag == true) {
    var data = [];
    data.push(fname);
    data.push(lname);
    data.push(email);
    data.push(phno);
    data.push(gender);
    data.push(state);
    data.push(cpassword);

    var string = JSON.stringify(data);
    console.log(string);
  }
});
